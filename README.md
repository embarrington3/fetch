# Powershell fetch tool

Add the contents of both the scripts to your powershell profile or add them to your system's PATH.

## Adding to PATH: 

- Search environment in the windows search bar.
- Click environment variables
- Find path and press edit
- Press browse and find the location of the src directory or paste the scripts into a directory of your choice that is added to the path.
- Restart powershell
