
function fetch() {
 $osName = (Get-WmiObject Win32_OperatingSystem).Caption
 $cpuName =  (Get-WmiObject Win32_Processor).Name
 $hostName = hostname
 $shell = $PSVersionTable.PSVersion

 Write-Host -ForegroundColor: Blue "______________________________"
 Write-Host -ForegroundColor: Blue "OS: $osName "
 Write-Host -ForegroundColor: Blue "CPU: $cpuName "
 Write-Host -ForegroundColor: Blue "User: $hostName"
 Write-Host -ForegroundColor: Blue "Powershelll Version: $shell"
 Write-Host -ForegroundColor: Blue "Uptime: $(uptime days):$(uptime hours):$(uptime minutes)"
 Write-Host -ForegroundColor: Blue "______________________________"
 

}
